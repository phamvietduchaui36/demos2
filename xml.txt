﻿<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="#188038"
        android:gravity="center"
        android:text="Mô tả đồng hồ"
        android:textAllCaps="true"
        android:textColor="@android:color/white"
        android:textSize="25sp" />

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <TextView
            android:id="@+id/textView"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"
            android:text="Mã ĐH" />

        <EditText
            android:id="@+id/txtma"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="6"
            android:ems="10"
            android:inputType="textPersonName"></EditText>

    </LinearLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <TextView
            android:id="@+id/textView2"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"
            android:text="Tên ĐH" />

        <EditText
            android:id="@+id/txtten"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="6"
            android:ems="10"
            android:inputType="textPersonName" />
    </LinearLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <TextView
            android:id="@+id/textView3"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"
            android:text="Loại ĐH" />

        <RadioGroup
            android:id="@+id/radioloaidh"
            android:layout_width="1dp"
            android:layout_height="match_parent"
            android:layout_weight="6"
            android:orientation="horizontal">

            <RadioButton
                android:id="@+id/rdnam"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="Nam" />

            <RadioButton
                android:id="@+id/rdnu"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Nữ" />
        </RadioGroup>
    </LinearLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <TextView
            android:id="@+id/textView5"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"
            android:text="Dây" />

        <RadioGroup
            android:id="@+id/radioday"
            android:layout_width="1dp"
            android:layout_height="match_parent"
            android:layout_weight="6"
            android:orientation="horizontal">

            <RadioButton
                android:id="@+id/rdda"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="Da" />

            <RadioButton
                android:id="@+id/rdkimloai"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="Kim loại" />

            <RadioButton
                android:id="@+id/rdkhac"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Khác" />
        </RadioGroup>
    </LinearLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="horizontal">

        <TextView
            android:id="@+id/textView6"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="3"
            android:text="Khuyến mãi" />

        <CheckBox
            android:id="@+id/checkBox"
            android:layout_width="1dp"
            android:layout_height="wrap_content"
            android:layout_weight="6" />
    </LinearLayout>

    <ListView
        android:id="@+id/listview"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />

</LinearLayout>